const Icons={
    ic_badge:require("./Icons/badge.png"),
    ic_connections:require("./Icons/connections.png"),
    ic_event:require("./Icons/event.png"),
    ic_home:require("./Icons/home.png"),
    ic_setting:require("./Icons/setting.png"),
    ic_badge_selected:require("./Icons/badge_selected.png"),
    ic_connections_selected:require("./Icons/connections_selected.png"),
    ic_event_selected:require("./Icons/event_selected.png"),
    ic_home_selected:require("./Icons/home_selected.png"),
    ic_setting_selected:require("./Icons/setting_selected.png"),
}
export default Icons;