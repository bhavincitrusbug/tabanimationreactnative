import React, { PureComponent } from "react";
import { View } from "react-native";

class TabScreen2 extends PureComponent {
  render() {
    return <View style={{ flex: 1, backgroundColor: "green" }} />;
  }
}
export default TabScreen2;
