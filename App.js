import React, { PureComponent } from "react";
import { StyleSheet, View } from "react-native";
import TabScreen1 from "./App/Screens/TabScreen1";
import TabScreen2 from "./App/Screens/TabScreen2";
import TabScreen3 from "./App/Screens/TabScreen3";
import TabScreen4 from "./App/Screens/TabScreen4";
import TabScreen5 from "./App/Screens/TabScreen5";
import Icons from "./App/Resources/Icons";
import TabBar from "@mindinventory/react-native-tab-bar-interaction";

export default class App extends PureComponent {
  render() {
    return (
      <View style={styles.container}>
        <TabBar>
          <TabBar.Item
            icon={Icons.ic_home}
            selectedIcon={Icons.ic_home_selected}
            title="Home"
          >
            <View style={{ flex: 1 }}>
              <TabScreen1 />
            </View>
          </TabBar.Item>
          <TabBar.Item
            icon={Icons.ic_connections}
            selectedIcon={Icons.ic_connections_selected}
            title="Connections"
          >
            <View style={{ flex: 1 }}>
              <TabScreen2 />
            </View>
          </TabBar.Item>
          <TabBar.Item
            icon={Icons.ic_event}
            selectedIcon={Icons.ic_event_selected}
            title="Events"
          >
            <View style={{ flex: 1 }}>
              <TabScreen3 />
            </View>
          </TabBar.Item>
          <TabBar.Item
            icon={Icons.ic_badge}
            selectedIcon={Icons.ic_badge_selected}
            title="Badge"
          >
            <View style={{ flex: 1 }}>
              <TabScreen4 />
            </View>
          </TabBar.Item>
          <TabBar.Item
            icon={Icons.ic_setting}
            selectedIcon={Icons.ic_setting_selected}
            title="Setting"
          >
            <View style={{ flex: 1 }}>
              <TabScreen5 />
            </View>
          </TabBar.Item>
        </TabBar>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: "center",
    alignItems: "center",
    backgroundColor: "white"
  },
  welcome: {
    fontSize: 20,
    textAlign: "center",
    margin: 10
  },
  instructions: {
    textAlign: "center",
    color: "#333333",
    marginBottom: 5
  }
});
